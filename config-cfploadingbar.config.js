(function () {
    'use strict';

    // CfpLoadingBar Provider
    function configCfpLoadingBar (cfpLoadingBarProvider) {

        cfpLoadingBarProvider.includeSpinner = false;

    }

    configCfpLoadingBar.$inject =['cfpLoadingBarProvider'];

    angular
        .module('bravoureAngularApp')
        .config(configCfpLoadingBar);

})();
